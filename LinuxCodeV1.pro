TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c++11

SOURCES += main.cpp \
    include/rs232.c

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    include/rs232.h


unix|win32: LIBS += -lncurses
