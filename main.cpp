#include <iostream>
#include <include/rs232.h>
#include <unistd.h>
#include <curses.h>
#include <ncurses.h>
#include <string>
#include <cstring>


using namespace std;

void SetSpeed(unsigned char speed);
void Loop();
void Deccelerate(int &speed);
void Accelerate(int &speed);
void SetTurn(char direction);
void Left(int &direction);
void Right(int &direction);

const int BaudRate = 9600;
//ComPort 24 is /dev/ACM0
const int ComPort = 24;
//ComPort 25 is /dev/ACM1
//const int ComPort = 25;


int main()
{
  RS232_OpenComport(ComPort,BaudRate);
  //Zero Speed and Direction before user has the ability to do anything
  unsigned char speed = 0;
  unsigned char direction = 0;
  SetSpeed(speed);
  SetTurn(direction);
  cout << "All motors have been stopped";
  sleep(1);
  Loop();
  RS232_CloseComport(ComPort);
  cout << endl;
  return 0;
}
void SetSpeed(unsigned char speed)
{
  //This will process speed and send it to the arduino
  move(2,11);
  addch(speed);
  refresh();
  RS232_SendByte(ComPort,'S');
  sleep(0.01);
  RS232_SendByte(ComPort,speed);
}
void SetTurn(char direction)
{
  //This will process the left or right turn and send it to the arduino
  move(3,11);
  addch(direction);
  refresh();
  RS232_SendByte(ComPort,'T');
  sleep(0.01);
  RS232_SendByte(ComPort,direction);
}

void Loop()
{
  int speed = 0; //Zero means stop, 255 means full speed
  int direction = 0; //Zero means as far as the system is concerned it's going straight, -127 (Left) to 127 (Right).
  bool exit=0;

  //Begining display software
  initscr();
  keypad(stdscr, TRUE);
  noecho();
  move(0,0);
  addstr("Welcome to Oregon Tech Robotics Club Robotics Control System");
  move(1,0);
  addstr("Version 0.1 Copyleft 2014");
  move(2,0);
  addstr("Speed: ");
  move(3,0);
  addstr("Direction: ");
  move(10,0);
  addstr("Press 'q' to quit");
  refresh();

  while (exit==0)
    {
      int key=getch();
      switch(key) {
        case KEY_UP:
          {
            Accelerate(speed);

            break;
          }
        case KEY_DOWN:
          {
            Deccelerate(speed);
            break;
          }
        case KEY_RIGHT:
          {
            Right(direction);
            break;
          }
        case KEY_LEFT:
          {
            Left(direction);
            break;
          }
        case '0':
          {
            SetSpeed(0);
            speed=0;
            break;
          }
        case '1':
          {
            SetSpeed(26);
            speed=26;
            break;
          }
        case '2':
          {
            SetSpeed(51);
            speed=51;
            break;
          }
        case '3':
          {
            SetSpeed(77);
            speed=77;
            break;
          }
        case '4':
          {
            SetSpeed(102);
            speed=102;
            break;
          }
        case '5':
          {
            SetSpeed(127);
            speed=127;
            break;
          }
        case '6':
          {
            SetSpeed(153);
            speed=153;
            break;
          }
        case '7':
          {
            SetSpeed(179);
            speed=179;
            break;
          }
        case '8':
          {
            speed=204;
            SetSpeed(204);
            break;
          }
        case '9':
          {
            speed=230;
            SetSpeed(230);
            break;
          }
        case 'q':
          {
            exit=1;
            endwin();
            break;
          }
        }

    }
}

//Turn left by one unit
void Left(int &direction)
{
  if (direction != -127)
    {
      direction--;
      SetTurn((char)direction);
    }
}
//Turn right by one unit
void Right(int &direction)
{
  if (direction != 127)
    {
      direction++;
      SetTurn((char)direction);
    }
}

//Deccelerate the current speed of the motor by one
void Deccelerate(int &speed)
{
  if (speed !=0)
    {
      speed--;
      SetSpeed((unsigned char)speed);
    }
}
//Accelerate the current speed of the motor by one
void Accelerate(int &speed)
{
  if (speed != 255)
    {
      speed++;
      SetSpeed((unsigned char)speed);
    }
}
